require 'json'
require 'markdown-tables'

file_name = "sca-scan.json"

#noinspection RubyLiteralArrayInspection
vulnerabilities_columns = [
  "Disclosure Date",
  "Title",
  "Overview",
  "Language",
  "Vulnerability Types",
  "Update To Version",
  "Links"
]

if File.exist?(file_name)
  file = File.open(file_name)
  json = JSON.parse(file.read())

  puts json # print whole json, for CI debugging

  root = json["records"][0]
  vulnerabilities = root["vulnerabilities"]

  if vulnerabilities.length < 1
    file.close
    puts "No sca vulnerabilities"
    return
  end

  vulnerabilities = vulnerabilities.map { |item| # item is a json object
    vulnerabilities_columns.map { |column|
      case column
      when "Disclosure Date" then item["disclosureDate"]
      when "Title" then item["title"]
      when "Overview" then item["overview"]
      when "Language" then item["language"]
      when "Vulnerability Types" then item["vulnerabilityTypes"]
      when "Update To Version" then item["libraries"][0]["details"][0]["updateToVersion"]
      when "Links" then item["_links"]["html"]
      else ""
      end
    }
  }

  table = MarkdownTables.make_table(vulnerabilities_columns, vulnerabilities, is_rows: true)

  result = %{
## Veracode sca scan
scan #{root["metadata"]["report"]}

#{table}
  }.strip

  markdown(result)

  file.close

end