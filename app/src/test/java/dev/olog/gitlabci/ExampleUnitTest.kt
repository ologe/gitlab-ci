package dev.olog.gitlabci

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun app_addition_isCorrect() {
        assertEquals(3, 2 + 2)
    }

}