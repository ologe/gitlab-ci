require 'nokogiri'

lint_dir = "**/reports/lint-result*.xml"

nodes = Dir[lint_dir].map { |item|
  f = File.open(item)
  xml = Nokogiri.XML(f)
  f.close
  xml.xpath("//issue")
}.flatten

result = %{
<issues>
#{nodes.map { |item| item.to_s }.join("\n")}
</issues>
}.strip

File.write("lint-results.xml", result)