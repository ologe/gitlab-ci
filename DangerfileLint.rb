require 'nokogiri'

merged_lint_results = "lint-results.xml"

# lint generates one file only from gradle 7.1
def merge_lint_files(merge_to)
  lint_dir = "**/reports/lint-result*.xml"

  nodes = Dir[lint_dir].map { |item|
    f = File.open(item)
    xml = Nokogiri.XML(f)
    f.close
    xml.xpath("//issue")
  }.flatten

  result = %{
<issues>
#{nodes.map { |item| item.to_s }.join("\n")}
</issues>
}.strip

  File.write(merge_to, result)
end

merge_lint_files(merged_lint_results)

android_lint.skip_gradle_task = true # is runned by CI
android_lint.filtering = true # show reports only for files changes in the current MR
android_lint.report_file = merged_lint_results
android_lint.lint