require 'json'
require 'markdown-tables'


file_name = "filtered_results.json"

if File.exist?(file_name)
  file = File.open(file_name)
  json = JSON.parse(file.read())

  findings = json["findings"]
  if findings.length < 1
    file.close
    puts "No pipeline scan findings"
  end

  #noinspection RubyLiteralArrayInspection
  labels = [
    "Title",
    "Severity",
    "Issue type id",
    "Issue type",
    "Description",
    "Files",
    "Flaw details link",
  ]
  data = findings.map { |item|
    labels.map { |column|
      case column
      when "Title" then item["title"]
      when "Severity" then item["severity"]
      when "Issue type id" then item["issue_type_id"]
      when "Issue type" then item["issue_type"]
      when "Description" then item["display_text"]
      when "Files" then item["files"]
      when "Flaw details link" then item["flaw_details_link"]
      else ""
      end
    }
  }

  table = MarkdownTables.make_table(labels, data, is_rows: true)

  result = %{
## Veracode pipeline scan
scan: #{json["_links"]["self"]["href"]}

  #{table}
  }.strip

  begin
    markdown(result)
  rescue
    puts result
  end

  file.close
end