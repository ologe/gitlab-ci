package dev.olog.another_module

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun another_module_addition_isCorrect() {
        assertEquals(3, 2 + 2)
    }
}